import * as THREE from "three";

export class Planet extends THREE.Group {

  body = new THREE.Mesh();
  clouds = new THREE.Mesh();
  radius: number;

  constructor(radius: number) {

    super();
    this.radius = radius;
    const bodyGeometry = new THREE.SphereGeometry(this.radius, 100, 100);

    const bodyMaterial = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load("assets/2_no_clouds_2k.jpg"),
      color: 0xaaaaaa,
      specular: 0x333333,
    });

    //Build earth mesh using our geometry and material
    this.body = new THREE.Mesh(bodyGeometry, bodyMaterial);

    new THREE.TextureLoader().load("assets/2_no_clouds_8k.jpg", (texture) => {
      console.log("LOADED");
      bodyMaterial.map = texture;
    });

    this.add(this.body);
    const cloudGeometry = new THREE.SphereGeometry(this.radius + 70, 100, 100);

    const cloudMaterial = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load("assets/fair_clouds_4k.png"),
      opacity: 0.3,
      transparent: true,
      blending: THREE.AdditiveBlending
    });

    this.clouds = new THREE.Mesh(cloudGeometry, cloudMaterial);
    this.add(this.clouds);
  }

  /**
   * Animation loop for the rendered planets.
   * Called by the THREE animate loop.
   * @param time Global Time
   */
  public animate(time: number, offsetPosition: THREE.Vector3): void {
    this.clouds.rotateY(0.00002);
  }

}
