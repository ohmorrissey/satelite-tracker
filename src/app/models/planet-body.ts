import { SatRec } from "satellite.js";

export interface ISatellite {
  name: string;
  epoch: number,
  satRec: SatRec
}

