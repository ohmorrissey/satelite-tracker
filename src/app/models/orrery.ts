import { SatRec, twoline2satrec } from "satellite.js";
import { ISatellite } from "./planet-body";

/**
 * Calculates mean anomoly from period and time
 * @param period Time to complete a full orbit
 * @param time Current simulation time (Same units as period)
 * @returns in radians
 */
export function calcMeanAnomoly(startingMeanAnomoly: number, period: number, epoch: number): number {
  const elapsedTime = Date.now() - epoch;
  let meanAnomoly = - 2 * Math.PI * elapsedTime / (period * 1000)
  meanAnomoly = (meanAnomoly + startingMeanAnomoly) % (2 * Math.PI);
  console.log("MeanAnom", meanAnomoly);
  return meanAnomoly;
}

/**
 * Calculates eccentric anomoly given mean anomoly and eccentricity using Newton-Raphson iteration.
 * @param meanAnomoly in radians
 * @param eccentricity scalar [0 < ecc < 1]
 * @param tollerance optional, in radians, default 1e-8
 * @returns eccentric anomaly in radians
 */
export function calcEccentricAnomonly(
  meanAnomoly: number,
  eccentricity: number,
  tollerance?: number): number {

  //https://au.mathworks.com/matlabcentral/fileexchange/6779-calce-m

  if (tollerance === undefined) {
    // default to 10^-8 rad tollerance
    tollerance = 0.00000001;
  }

  let eccentricAnomaly: number = meanAnomoly + 0.1;
  let error: number = 1;
  let n = 0;

  if (eccentricity >= 1) {
    throw new Error('Eccentricity greater or equal to 1');
  }

  while (Math.abs(error) > tollerance) {
    const tryE = eccentricAnomaly - eccentricity * Math.sin(eccentricAnomaly) - meanAnomoly;
    const tryEPlusOne = 1 - eccentricity * Math.cos(eccentricAnomaly);
    error = tryE / tryEPlusOne;
    eccentricAnomaly -= error;
    n += 1;
  }

  return eccentricAnomaly
}

/**
 * returns 2D Cartesian Coordinates of a body in an elliptical orbit.
 * @param semiMajorAxis
 * @param semiMinorAxis
 * @param eccentricity
 * @param eccentricAnomaly
 * @returns [x, y] coorinates in the same units as the semi major and minor axis
 */
export function eccentricAnomalyToCartesian(
  semiMajorAxis: number,
  semiMinorAxis: number,
  eccentricity: number,
  eccentricAnomaly: number): number[] {
  const x = semiMajorAxis * (Math.cos(eccentricAnomaly) - eccentricity);
  const y = semiMinorAxis * Math.sin(eccentricAnomaly)
  return [x, y];
}
/**
 *
 * @param meanMotion In radians per minute
 */
export function calcSemiMajor(meanMotion: number): number {
  const mu = 3.986E14;
  const angularVelocity = (meanMotion / 60); // convert to radians per second
  const num = Math.pow(mu, 1 / 3);
  const den = Math.pow(angularVelocity, 2 / 3);
  return (num / (den * 1000)) // Returns in km, not m

}

/**
 * Calculates semi minor axis from semi major and eccentricity
 * @param semiMajorAxis
 * @param eccentricity
 * @returns semiMinorAxis
 */
export function calcSemiMinorAxis(semiMajorAxis: number, eccentricity: number): number {
  const eccSqrd = Math.pow(eccentricity, 2)
  const semiMinorAxis = semiMajorAxis * Math.sqrt((1 - eccSqrd));
  return semiMinorAxis;
}

export function calcApogee(semiMajor: number, eccentricity: number): number {
  return (semiMajor * (eccentricity + 1))
}

export function calcperigee(semiMajor: number, eccentricity: number): number {
  return (semiMajor * (1 - eccentricity))
}

export function satelliteFromTLE(name: string, tle1: string, tle2: string): ISatellite {

  const satRec: SatRec = twoline2satrec(tle1, tle2);

  const sat: ISatellite = {
    name: name,
    epoch: julianToDate(satRec.jdsatepoch),
    satRec: satRec,
  }

  return sat
}


export function julianToDate(julian: number): number {
  const unixMillis = (julian - 2440587.5) * (1000 * 60 * 60 * 24);
  console.log(unixMillis);
  return (unixMillis);
}
