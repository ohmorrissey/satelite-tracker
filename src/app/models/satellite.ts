import * as THREE from "three";
import { ISatellite } from "./planet-body";
import { PositionAndVelocity, SatRec, eciToEcf, gstime, propagate } from "satellite.js";

export class Satellite extends THREE.Group implements ISatellite {

    mesh: THREE.Mesh | undefined;

    sprite = new THREE.Sprite();
    spriteText = new THREE.Sprite();
    orbitLine = new THREE.Line();

    earthLine = new THREE.Line(undefined, new THREE.LineBasicMaterial({
        color: 0xaaaaaa
    }));


    epoch: number;
    satRec: SatRec;

    constructor(satilliteIn: ISatellite) {

        super();

        this.name = satilliteIn.name;
        this.epoch = satilliteIn.epoch;
        this.satRec = satilliteIn.satRec;

        const map = new THREE.TextureLoader().load('assets/satellite_icon.png');
        const material = new THREE.SpriteMaterial({ map: map });
        this.sprite = new THREE.Sprite(material);
        this.setSpriteScale(400);
        this.setSatellitePositionSPG4(Date.now());
        this.add(this.sprite);
        this.drawOrbitLine();
        this.drawEarthLine();
        this.add(this.orbitLine);
        this.add(this.earthLine);
    }

    /**
     * Animation loop for the rendered planets.
     * Called by the THREE animate loop.
     * @param time Global Time
     */
    public animate(time: number, offsetPosition: THREE.Vector3): void {
        this.setSatellitePositionSPG4(time);
        this.position.add(offsetPosition);
        this.drawEarthLine();
    }

    private setSatellitePositionSPG4(time: number): void {
        const eciPosAndVel: PositionAndVelocity = propagate(this.satRec, new Date(Date.now()));

        if (eciPosAndVel.position == true || eciPosAndVel.position == false) {
            console.warn("weird")
            return;
        }

        const ecf = eciToEcf(eciPosAndVel.position, gstime(new Date(Date.now())))
        this.sprite.position.set(ecf.x, ecf.z, - ecf.y);
        this.spriteText.position.set(ecf.x, ecf.z, - ecf.y);
    }

    drawOrbitLine() {
        const points: THREE.Vector3[] = [];

        const material = new THREE.LineBasicMaterial({
            color: 0xaaaaaa
        });

        for (let i = 0; i < 180; i++) {
            const date = new Date(Date.now() + (i * 1000 * 30) - 45 * 60 * 1000)
            const eci = propagate(this.satRec, date);

            if (eci.position == true || eci.position == false) {
                console.warn("weird")
                return;
            }

            const ecf = eciToEcf(eci.position, gstime(date))
            const point = new THREE.Vector3(ecf.x, ecf.z, -ecf.y);
            points.push(point);
        }

        this.orbitLine.geometry = new THREE.BufferGeometry().setFromPoints(points);
        setTimeout(() => { this.drawOrbitLine() }, 30 * 1000);

    }

    private drawEarthLine() {
        const points: THREE.Vector3[] = [];
        points.push(new THREE.Vector3(0, 0, 0));
        points.push(this.sprite.position);
        this.earthLine.geometry.setFromPoints(points);
    }

    public setSpriteScale(scale: number) {
        this.sprite.scale.set(scale, scale, 1);
        this.spriteText.scale.set(scale, scale, scale);
    }
}
