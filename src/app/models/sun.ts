import * as THREE from "three";

export class Sun extends THREE.Group {

    ambientLight = new THREE.AmbientLight(0x888888, 0.3);
    sunLight = new THREE.PointLight(0xfdfcf0, 1);

    constructor() {
        super();
        this.sunLight.position.set(-200000, 200000 * this.getSunlightSeason(new Date(Date.now())), 0);
        this.add(this.ambientLight)
        this.add(this.sunLight);
    }

    private getSunlightSeason(date: Date) {
        const day = date.getUTCDate();
        const month = date.getUTCMonth();
        const year = month * (365 / 12) + (day);
        const angle = 23 * (Math.PI / 180) * Math.sin((year - 79) * 2 * Math.PI);
        return angle;
    }

    /**
    * Calculates the planet angle from orbital data and global time.
    * @param time Global Time
    * @returns Angle of the planet (Radians)
    */
    private getSunlightPosition(date: Date): number {
        const hours = date.getUTCHours();
        const minutes = date.getUTCMinutes();
        const seconds = date.getUTCSeconds();
        const milliseconds = date.getUTCMilliseconds();
        const days = (hours * 60 + minutes + (seconds / 60) + (milliseconds / (60 * 1000))) / (24 * 60);
        const angle = Math.PI * 2 * (days);
        return angle
    }

    public animate() {
        this.sunLight.position.set(-200000, 200000 * this.getSunlightSeason(new Date(Date.now())), 0);
        const eular = new THREE.Euler(0, -this.getSunlightPosition(new Date(Date.now())), 0);
        this.sunLight.position.applyEuler(eular);
    }
}