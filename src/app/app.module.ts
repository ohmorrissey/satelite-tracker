import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ThreeService } from './services/THREE.service';


@NgModule({
  imports: [BrowserModule, FormsModule],
  providers: [ThreeService],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
