import { Component, OnInit } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from "@three-ts/orbit-controls"
import { OrbitalBodyService } from './services/orbit-body.service';
import { ThreeService } from './services/THREE.service';

const DEBUG_MODE = true;

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  name = 'kerbol-viewer';
  scene = new THREE.Scene();
  startTime: number = Date.now();
  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100000000);
  renderer = new THREE.WebGLRenderer();
  orbitControls = new OrbitControls(this.camera, this.renderer.domElement);
  debug: boolean = DEBUG_MODE;
  axis = new THREE.AxesHelper(1);

  constructor(
    private orbitalBodyService: OrbitalBodyService,
    public threeService: ThreeService
  ) { }


  ngOnInit() {

    this.threeService.nextFrame.subscribe((time) => {
      const origin = new THREE.Vector3(0, 0, 0);
      const camDistToOrigin = this.threeService.camera.position.distanceTo(origin) / 5;
      this.axis.scale.set(camDistToOrigin, camDistToOrigin, camDistToOrigin);
    })

    if (this.debug) {
      // this.threeService.addToScene('axis', this.axis);
    }

  }

  /**
   * Runs when console.log button pressed. Purely for clean debugging purposes.
   */
  onClickConsoleLog(): void {
    console.log(this.camera.position.x, this.camera.position.y, this.camera.position.z);
  }

  /**
   * runs on click of debug button
   */
  onClickDebug(): void {

    if (this.debug && !this.scene.getObjectByName('axis')) {
      // this.threeService.addToScene('axis', this.axis);
      console.log('Adding Axis');
    }
    if (!this.debug && this.scene.getObjectByName('axis')) {
      // this.threeService.removeFromScene('axis');
      console.log('Removing Axis');
    }
  }
}
