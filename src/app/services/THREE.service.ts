import { Injectable } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Subject } from 'rxjs';

/**
 * Service that handles the overall THREE rendering.
 */
@Injectable()
export class ThreeService {
    /** Is Three Service Initiated? */
    public ThreeRunning: boolean = false;
    private objects = new Map<string, THREE.Object3D<THREE.Event>>();
    public scene = new THREE.Scene();
    public camera = new THREE.PerspectiveCamera(
        30,
        window.innerWidth / window.innerHeight,
        1000,
        10000000
    );
    public renderer = new THREE.WebGLRenderer({ antialias: true });
    public orbitControls = new OrbitControls(
        this.camera,
        this.renderer.domElement
    );

    /** Time between frames. */
    private timeDiff = new Subject<number>();
    /** Triggered on new frame and passes the time between last frame. */
    public nextFrame = this.timeDiff.asObservable();

    constructor(
    ) {
        console.log('Three Service Started');
        this.init();
        this.orbitControls.minDistance = 1100;
        this.orbitControls.maxDistance = 50000;
    }

    /**
     * Initiate THREE Service. Loads THREE into webpage body. Loads water and lights.
     * Runs THREE animation.
     * @returns
     */
    init(): void {
        if (this.ThreeRunning) {
            // console.warn('ThreeService already running');
            return;
        }
        console.log('ThreeService Spooling');

        this.ThreeRunning = true;

        this.renderer.autoClear = false;

        this.initialiseWindow();
        this.initialiseScene();
        this.startAnimation();
    }

    public focusOn(object: THREE.Object3D): void {
        this.orbitControls.target = object.position;
    }

    /**
     * Starts core THREE aspects, camera, renderer etc.
     */
    initialiseWindow(): void {
        /** Set initial camera and window settings */
        this.camera.position.set(20000, 20000, 0);
        this.camera.rotation.set(0, Math.PI / 4, 0);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.camera.layers.enableAll();

        //Add the renderer canvas to the DOM.
        document.body.appendChild(this.renderer.domElement);

        /**
         * Resizes THREE scene to the full window.
         */
        const WindowResize = () => {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize(window.innerWidth, window.innerHeight);
        };

        // Listen for a window resize, then resize THREE renderer accordingly
        window.addEventListener('resize', WindowResize, false);
        window.addEventListener('orientationchange', WindowResize);
    }

    /**
     * Starts the THREE Scene related objects, lights, water etc.
     */
    private initialiseScene(): void {
        // Add objects that were added before THREE was initialised.
        for (let [name, obj] of this.objects) {
            console.warn('Adding from Stored: ', name);
            this.scene.add(obj);
        }
    }

    /**
     * Add an Object to THREE Scene.
     * @param name local name of object.
     * @param object object instance
     */
    public addToScene(name: string, object: THREE.Object3D<THREE.Event>): void {
        // If three isn't running, return
        if (!this.ThreeRunning) {
            console.error('Loaded before THREE spooled ', name);
            return;
        }

        object.name = name;
        this.objects.set(name, object);
        this.scene.add(object);
    }

    public removeFromScene(name: string): void {
        const object = this.objects.get(name);
        if (!object) {
            return;
        }

        this.scene.remove(object);

        this.objects.delete(name);
    }

    /** THREE Animation Loop */
    private startAnimation(): void {
        // Part of initialising orbital controls.
        this.orbitControls.update();

        /**
         * Main loop for THREE animation
         */
        const animateTHREE = () => {
            this.timeDiff.next(Date.now());
            this.orbitControls.update();
            requestAnimationFrame(animateTHREE);
            this.renderer.render(this.scene, this.camera);
        };

        // Render First frame
        this.renderer.render(this.scene, this.camera);
        animateTHREE();
    }
}
