import { TestBed } from '@angular/core/testing';

import { OrbitalBodyService } from './orbit-body.service';

describe('OrbitalBodyService', () => {
  let service: OrbitalBodyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrbitalBodyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
