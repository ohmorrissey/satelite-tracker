import { Injectable } from '@angular/core';
import * as THREE from 'three';
import { __values } from 'tslib';
import { Planet } from '../models/planet';
import { ThreeService } from './THREE.service';
import { satelliteFromTLE } from '../models/orrery';
import { Satellite } from '../models/satellite';
import { Sun } from '../models/sun';

const STAR_FIELD_MAP_PATH = 'https://i.imgur.com/kXirxkP.png'; // Original

@Injectable({
  providedIn: 'root'
})

/**
 * Handles the rendering of planets=
 */
export class OrbitalBodyService {

  constructor(
    private threeService: ThreeService
  ) {
    this.renderStars();
    this.renderSun();
    this.renderEarth();
    this.renderSatellites();

    this.threeService.nextFrame.subscribe((time) => {
      this.animate(time);
    });
  }

  sat?: Satellite;
  earth: Planet = new Planet(6371);
  sunLight = new Sun();

  /**
   * THREE.Mesh of stars
   */
  starField: THREE.Mesh = new THREE.Mesh();

  /**
   * Renders inside out star map to THREE Scene
   * @param scene THREE Scene to add stars to
   */
  public renderStars(): void {
    const starGeometry = new THREE.SphereGeometry(1000000, 50, 50);
    const starMaterial = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load(STAR_FIELD_MAP_PATH),
      side: THREE.BackSide,
      shininess: 0
    });
    this.starField = new THREE.Mesh(starGeometry, starMaterial);
    this.threeService.addToScene('starfield', this.starField);
  }

  public renderSun(): void {
    this.threeService.addToScene('ambientLight', this.sunLight);

  }

  public async renderSatellites(): Promise<void> {

    let tle: string[];
    const raw = await fetch("https://celestrak.org/NORAD/elements/gp.php?CATNR=56981&FORMAT=tle");

    if (raw.status !== 200) {
      tle = [
        "SKYKRAFT-3A",
        "1 56981U 23084BB  23202.75584389  .00014531  00000+0  80710-3 0  9996",
        "2 56981  97.5158 318.8016 0013217  83.6947 276.5790 15.13748109  5880"
      ]

      console.warn("Cannot get Skykraft-3 TLE. Using historical Data from 23/7/2023");
    } else {
      const text = await raw.text();
      tle = text.split(/\r?\n/);
    }

    console.log(tle);
    const skykraftConfig = satelliteFromTLE(tle[0], tle[1], tle[2]);
    this.sat = new Satellite(skykraftConfig);
    this.threeService.addToScene(this.sat.name, this.sat);
    this.threeService.focusOn(this.sat.sprite);

    this.threeService.camera.position.set(
      2 * this.sat.sprite.position.x,
      this.sat.sprite.position.y,
      3 * this.sat.sprite.position.z,
    );
  }

  public renderEarth(): void {
    this.threeService.addToScene(`earth`, this.earth);
  }


  /**
   * Animation loop for the rendered planets.
   * Called by the THREE animate loop.
   * @param time Global Time
   */
  public animate(time: number): void {
    let offsetPosition = new THREE.Vector3(0, 0, 0);
    this.earth.animate(time, offsetPosition);
    this.sunLight.animate();
    this.calcSatScale();
    this.sat?.animate(time, offsetPosition);

  }


  getSunlightPosition(date: Date) {

    const day = date.getUTCDate();
    const month = date.getUTCMonth();
    const year = month * (365 / 12) + (day);

    const angle = 23 * (Math.PI / 180) * Math.sin((year - 79) * 2 * Math.PI);
    return angle;
  }

  calcSatScale() {
    if (!this.sat) {
      return;
    }
    const cameraPos = this.threeService.camera.position.clone();
    const positionDifference = cameraPos.sub(this.sat.sprite.position);
    const scale = this.sat.sprite.position.distanceTo(this.threeService.camera.position);
    this.sat.setSpriteScale(scale / 75);
  }

}
